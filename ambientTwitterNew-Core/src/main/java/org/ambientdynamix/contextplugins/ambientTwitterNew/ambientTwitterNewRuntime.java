/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientTwitterNew;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.*;
import org.ambientdynamix.contextplugins.ambientcontrol.*;

import java.util.Map;
import java.util.UUID;

/**
 * Example auto-reactive plug-in that detects the device's battery level.
 *
 * @author Darren Carlson
 */
public class ambientTwitterNewRuntime extends ContextPluginRuntime {
    private static final int VALID_CONTEXT_DURATION = 60000;
    private static final String ACTION_TWITTER_DIRECT_MESSAGE_RECIEVED="org.ambientdynamix.ambientTwitter.directmessage";
    // Static logging TAG
    private final String TAG = this.getClass().getSimpleName();
    // Our secure context
    private Context context;
    private ControlConnectionManager ccm;

    // A BroadcastReceiver variable that is used to receive twitter direct message status.
    private BroadcastReceiver ambientTwitterBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG,"twiiter broadcast is detected");
            sendBroadcastContextEvent(new MyBatteryLevelInfo(intent),
                    VALID_CONTEXT_DURATION);
            ccm.sendControllCommand(new ToggleCommand(Commands.SWITCH,true,"notify"),intent.getStringExtra("TWITTER_USER")); //TODO: (2)Do this in the App
            Log.i(TAG,"ccm command emitted");
        }
    };

   /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run. If initialization is unsuccessful, the plug-ins should throw an exception and release
     * any acquired resources.
     */
    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        // Set the power scheme
        Log.i(TAG,"ambient twitter run time init v0.02");
        this.setPowerScheme(powerScheme);
        // Store our secure context
        this.context = this.getSecuredContext();
        Log.i(TAG,"context obtained");
        this.ccm = new ControlConnectionManager(this,clientListner,serverListner,new TranslatingProfileMatcher(),TAG);
        Log.i(TAG,"ccm obtained");
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() {
        // Register for battery level changed notifications
        Log.i(TAG,"registering for twitter direct messages v0.02");
       context.registerReceiver(ambientTwitterBroadcastReceiver, new IntentFilter(ACTION_TWITTER_DIRECT_MESSAGE_RECIEVED));
        Log.d(TAG, "Started!");
    }

    @Override
    public IPluginView getDefaultConfigurationView() {
        Log.i(TAG, "Problem starts v63.0");
        String authURL = "https://www.google.com.sg";//beginAuthorization();

        IPluginView iPluginView = new twitterAuthorization(context, this, authURL,ccm);

        return iPluginView;
    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {
        // Unregister battery level changed notifications
        try {
            context.unregisterReceiver(ambientTwitterBroadcastReceiver);
        } catch (IllegalArgumentException e) {
//            do nothing, reeiver is not registered
        }
        Log.d(TAG, "Stopped!");
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        this.stop();
        context = null;
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {

        // Check for proper context type
        if (contextType.equalsIgnoreCase(MyBatteryLevelInfo.CONTEXT_TYPE)) {
            // Manually access the battery level with a null BroadcastReceiver
            Log.i(TAG,"context type recognized properly");
            Intent batteryIntent = context.registerReceiver(null, new IntentFilter(ACTION_TWITTER_DIRECT_MESSAGE_RECIEVED));
            Log.i(TAG,"registered for the twitter dm broadcast");
            // Send the context event
            sendContextEvent(requestId, new MyBatteryLevelInfo(batteryIntent), VALID_CONTEXT_DURATION);
            Log.i(TAG,"context event sent");
        } else {
            sendContextRequestError(requestId, "NO_CONTEXT_SUPPORT for " + contextType, ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
        }
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
        // Warn that we don't handle configured requests
        Log.w(TAG, "handleConfiguredContextRequest called, but we don't support configuration!");
        // Drop the config and default to handleContextRequest
        handleContextRequest(requestId, contextType);
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }


    private ControlConnectionManager.ControllConnectionManagerListenerAdapter serverListner= new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {

        @Override
        public void controllRequest(String command, String name) {
            Log.i(TAG,"command: " +command+" recieved with name "+name);
        }

        @Override
        public void stopControlling(String command, String name) {

        }
    };

    private ControlConnectionManager.ControllConnectionManagerListenerAdapter clientListner= new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {
        @Override
        public void controllRequest(String command, String name) {

        }

        @Override
        public void stopControlling(String command, String name) {

        }

        @Override
        public void consumeCommand(IControlMessage command, String sourcePluginId, String requestId) {

        }

        @Override
        public Map<String, String> provideFeedbackSuggestion() {
            return null;
        }
    };

    @Override
    public void onMessageReceived(Message message) {
        Log.i(TAG, "implemented onMessageReceived v0.01");
        if (message!=null) {
            try {
                ccm.handleConfigCommand(message);
            } catch (RemoteException e) {
                Log.w(TAG, e.toString());
            }
        }else{
            Log.i(TAG,"message is null");
        }
    }

    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
       // Bundle listenerConfig = listenerInfo.getListenerConfig();
        boolean isAdded = ccm.addContextListener(listenerInfo);

        return isAdded;
    }

}