package org.ambientdynamix.contextplugins.ambientTwitterNew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import org.ambientdynamix.api.application.ContextResult;
import org.ambientdynamix.api.application.ICallback;
import org.ambientdynamix.api.application.IContextRequestCallback;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.contextplugins.ambientcontrol.Commands;
import org.ambientdynamix.contextplugins.ambientcontrol.ControlConnectionManager;
import org.ambientdynamix.contextplugins.ambientcontrol.IControlMessage;
import org.ambientdynamix.contextplugins.ambientcontrol.ToggleCommand;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;


/**
 * Created by Nirandika Wanigasekara on 5/3/2015.
 */
public class twitterAuthorization implements IPluginView {

    private String TAG = this.getClass().getSimpleName();
    private Context context;
    private ContextPluginRuntime runtime;
    private String message;
    private ActivityController controller;
    private String consumerKey = "1cGPlvpTzQEIAUsixcDVAkY6k";
    private String consumerSecretKey = "zSHbDzNzkfCh9s1FfNw6gGf67EzcjzGSiKfDNzQF2eipl1oNY1";
    private WebView webView;
    private String TWITTER_OAUTH_VARIFIER = "oauth_verifier";
    private String CALLBACK_URL = "http://ambientdynamix.org";
    private RequestToken currentRequestToken;
    private Twitter twitter;
    private AccessToken accessToken;
    private ControlConnectionManager ccm;

    public twitterAuthorization(Context context, ContextPluginRuntime runtime, String message, ControlConnectionManager ccm) {
        this.ccm = ccm;
        Log.i(TAG, "twitterAuthorization");
        this.context = context;
        this.runtime = runtime;
        this.message = message;
        webView = new WebView(context);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "shouldOverrideURLLoading");
                if (url.startsWith("https://api.twitter.com")) {
                    Log.i(TAG, "received the correct url 0.10");
                    view.loadUrl(url);
                    return true;
                } else if (url.startsWith("http://ambientdynamix.org")) {

                    Log.i(TAG, "callback worked");
                    Uri uri = Uri.parse(url);
                    Log.i(TAG, "uri recived");
                    String verifier = uri.getQueryParameter(TWITTER_OAUTH_VARIFIER);
                    Log.i(TAG, "verifier obtained " + verifier);
                    new TwitterGetAccessToken().execute(verifier, url);

                }
                return false;
            }

        });
    }

    @Override
    public void setActivityController(ActivityController activityController) {
        this.controller = activityController;
    }

    /*Creats a web view to display the twitter login page*/
    @Override
    public View getView() { //TODO: Check with Darren if its possible to pass the layout xml file
        Log.i(TAG, "the web view for Twitter login");
        // Discover our screen size for proper formatting
        DisplayMetrics met = context.getResources().getDisplayMetrics();
        Log.i(TAG, "Screen Size (WxH): " + met.widthPixels + "x" + met.heightPixels);

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        Log.i(TAG, "ConfigurationBuilder");
        configurationBuilder.setOAuthConsumerKey(consumerKey);
        configurationBuilder.setOAuthConsumerSecret(consumerSecretKey);
        //configurationBuilder.setUseSSL(true);
        Log.i(TAG, "values set");
        Configuration configuration = configurationBuilder.build();


        //PropertyConfiguration configuration= new PropertyConfiguration(properties,classLoader.toString());
        Log.i(TAG, "property configuration done");
        if (configuration != null) {
            Log.i(TAG, "configuration is not null");
            twitter = new TwitterFactory(configuration).getInstance();
            //  twitter.setOAuthConsumer(consumerKey,consumerSecretKey);
            new TwiiterLogin().execute();

        } else {
            Log.i(TAG, "configuration is null");
        }

        // Access our Locale via the incoming context's resource configuration to determine language
        String language = context.getResources().getConfiguration().locale.getDisplayLanguage();
        Log.i(TAG, "Languate: " + language);

        // Create a relative layout
        RelativeLayout relativeLayout = new RelativeLayout(context); //TODO: Did not set the orientation.
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));

        //load the default url

        if (message != null) {
            webView.loadUrl(message);
        } else {
            Log.i(TAG, "Authorized URL form Twitter is null");
            destroyView();
        }

        webView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        relativeLayout.addView(webView);


        return relativeLayout;
    }

    @Override
    public void destroyView() {
        controller.closeActivity();
    }

    @Override
    public void handleIntent(Intent intent) { //TODO: Think of a way to use this

    }

    @Override
    public int getPreferredOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }


    public class TwiiterLogin extends AsyncTask<Void, Void, String> {

        private String TAG = getClass().getSimpleName();

        @Override
        protected String doInBackground(Void... voids) {

            String authURL = "";
            try {
                currentRequestToken = twitter.getOAuthRequestToken();
                Log.i(TAG, "request token obtained");
                if (currentRequestToken != null) {
                    Log.i(TAG, "request token is not null");
                    authURL = currentRequestToken.getAuthenticationURL();
                    Log.i(TAG, authURL);
                } else {
                    Log.i(TAG, "current request token is null");
                }

            } catch (TwitterException e) {
                e.printStackTrace();
            }
            return authURL;
        }

        @Override
        protected void onPostExecute(String authURL) {
            Log.i(TAG, authURL + " recieved");
            webView.loadUrl(authURL);
            Log.i(TAG, "URL loaded");
        }
    }

    public class TwitterGetAccessToken extends AsyncTask<String, Void, String> {

        private String TAG = getClass().getSimpleName();

        @Override
        protected String doInBackground(String[] verifier) {
            try {
                accessToken = twitter.getOAuthAccessToken(currentRequestToken, verifier[0]);
                //configurationBuilder.setOAuthAccessToken(accessToken.getToken()); //TODO:See why there is exception here.
               // configurationBuilder.setOAuthAccessTokenSecret(accessToken.getTokenSecret());
                Log.i(TAG, "Access Token: " + accessToken.getToken());
                Log.i(TAG, "Access secret token: 0.03" + accessToken.getTokenSecret());
                //TODO: Figure out what more to do here.

            } catch (TwitterException e) {
                e.printStackTrace();
            }
            return verifier[1];
        }

        @Override
        protected void onPostExecute(String authURL) {
            Log.i(TAG, authURL + " recieved 0.01");

            webView.loadUrl(authURL);
            Log.i(TAG, "URL loaded");
            startStreamingTimeLine();
        }
    }

    private void startStreamingTimeLine() {
        Log.i(TAG,"started streaming time line 0.02");
        ConfigurationBuilder configurationBuilder2= new ConfigurationBuilder();
        configurationBuilder2.setOAuthConsumerKey(consumerKey);
        configurationBuilder2.setOAuthConsumerSecret(consumerSecretKey);
        configurationBuilder2.setOAuthAccessToken(accessToken.getToken());
        configurationBuilder2.setOAuthAccessTokenSecret(accessToken.getTokenSecret());
        Log.i(TAG,"Access token set");
        TwitterStream twitterStream= new TwitterStreamFactory(configurationBuilder2.build()).getInstance();
        Log.i(TAG,"twitter stream set");
        UserStreamListener userStreamListener= new UserStreamListener() {
            @Override
            public void onDeletionNotice(long l, long l2) {

            }

            @Override
            public void onFriendList(long[] longs) {

            }

            @Override
            public void onFavorite(User user, User user2, Status status) {

            }

            @Override
            public void onUnfavorite(User user, User user2, Status status) {

            }

            @Override
            public void onFollow(User user, User user2) {

            }

            @Override
            public void onUnfollow(User user, User user2) {

            }

            @Override
            public void onDirectMessage(DirectMessage directMessage) {
                Log.i(TAG,"Direct Message recieved "+directMessage.getText() );
                Intent intent= new Intent();
                intent.setAction("org.ambientdynamix.ambientTwitter.directmessage");
                intent.putExtra("TWITTER_DIRECT_MESSAGE", directMessage.getText());
                intent.putExtra("TWITTER_USER", directMessage.getRecipient().getName());
                //Activity activity= new Activity();

               // activity.sendBroadcast(intent);
                context.sendBroadcast(intent);
                Log.i(TAG,"name: "+directMessage.getRecipient().getName());
                /*ccm.sendControllCommand(new ToggleCommand(Commands.SWITCH,true,"notify"),directMessage.getRecipient().getName().toString()); //TODO: (2)Do this in the App
                Log.i(TAG,"ccm command emitted");*/


            }

            @Override
            public void onUserListMemberAddition(User user, User user2, UserList userList) {

            }

            @Override
            public void onUserListMemberDeletion(User user, User user2, UserList userList) {

            }

            @Override
            public void onUserListSubscription(User user, User user2, UserList userList) {

            }

            @Override
            public void onUserListUnsubscription(User user, User user2, UserList userList) {

            }

            @Override
            public void onUserListCreation(User user, UserList userList) {

            }

            @Override
            public void onUserListUpdate(User user, UserList userList) {

            }

            @Override
            public void onUserListDeletion(User user, UserList userList) {

            }

            @Override
            public void onUserProfileUpdate(User user) {

            }

            @Override
            public void onBlock(User user, User user2) {

            }

            @Override
            public void onUnblock(User user, User user2) {

            }

            @Override
            public void onStatus(Status status) {

            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

            }

            @Override
            public void onTrackLimitationNotice(int i) {

            }

            @Override
            public void onScrubGeo(long l, long l2) {

            }

            @Override
            public void onStallWarning(StallWarning stallWarning) {

            }

            @Override
            public void onException(Exception e) {

            }
        };
        twitterStream.addListener(userStreamListener);
        twitterStream.user();

        //TODO: Handle stoping the streaming. Check how draining it is to keep streaming like this?

    }



}
